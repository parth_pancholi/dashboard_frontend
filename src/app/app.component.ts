import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'dashboard';
  Chart: any;

  ngOnInit() {
    const media = window.matchMedia;
    if (media) {
      const mediaQueryList = window.matchMedia('print');
      mediaQueryList.addListener((mql) => {
        if (mql.matches) {
          this.beforePrint();
        }
      });
    }
    const event = this.beforePrint();
    window.onbeforeprint = event as any;

  }

  public beforePrint = () => {
    if (this.Chart) {
      for (const id in this.Chart.instances) {
        if (this.Chart.instances[id]) {
          this.Chart.instances[id].resize();
        }
      }
    }
  }
}
