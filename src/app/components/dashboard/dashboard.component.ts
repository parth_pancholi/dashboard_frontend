import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../../services/dashboard.service';
import { AppConfig } from '../../app.config';
import * as _ from 'lodash';
import * as Chart from 'chart.js';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constituencyList = [{ label: 'विकल्प विधानसभा', value: null }];
  selectedConstituency: any;

  boothList = [{ label: 'विकल्प बूथ क्रमाक', value: null }];
  selectedBooth: any;

  reportPrefix: any = '';

  showStatistics = false;
  genderData: any;
  genderChartData: any;
  surnameData: any;
  surnameChartData: any;
  sectionGenderChartData: any;
  ageVotersChartData: any;
  ageRangeVotersChartData: any;
  houseVotersData;
  rowGroupMetadata = {};

  genderOptions: any;
  surnameOptions: any;
  sectionGenderOptions: any;
  ageVotersOptions: any;
  ageRangeVotersOptions: any;

  showLoad = false;
  loading = false;

  cols = [
    { field: 'HouseNumber', header: 'घर' },
    { field: 'Surnameinenglish', header: 'मुख्य उपनाम' },
    { field: 'num', header: 'मतदाता की संख्या' }
  ];

  constructor(
    private _dashboardService: DashboardService,
    private _appConfig: AppConfig
  ) { }

  ngOnInit() {
    this._dashboardService.getMasterData({}).subscribe((data: any) => {
      data = data || [];
      const finalConstituencyList = data.map(constituency => {
        let booths = constituency.booth || [];
        booths = booths.map(booth => {
          return {
            label: booth, value: booth
          };
        });
        return {
          label: constituency.constituency,
          value: constituency.constituency,
          booths: booths
        };
      });
      this.constituencyList = this.constituencyList.concat(finalConstituencyList);
    }, error => {
      console.log('Error => ', error);
    });
  }

  fetchBoothForConstituency = (event) => {
    const constituency = _.find(this.constituencyList, ['value', event.value]);
    this.boothList = [{ label: 'विकल्प बूथ क्रमाक', value: null }];
    this.boothList = this.boothList.concat(constituency.booths);
  }

  showLoadButton = (event) => {
    if (!this.selectedConstituency) {
      return;
    }
    if (!this.selectedBooth) {
      return;
    }
    this.showLoad = true;
  }

  loadData = () => {

    if (!this.selectedConstituency) {
      return;
    }
    if (!this.selectedBooth) {
      return;
    }
    // call API
    let args = {};
    args = {
      constituency: this.selectedConstituency,
      booth: this.selectedBooth
    };

    this._dashboardService.getBoothDetails(args).subscribe(data => {
      this.reportPrefix = data ? data['reportPrefix'] || '' : '';
    }, error => {
      console.log('Error => ', error);
    });


    this._dashboardService.getGenderData(args).subscribe(data => {
      this.showStatistics = true;
      this.genderData = data;
      const chartData = this.generateChartData(this.genderData, 'gender');

      this.genderChartData = {
        labels: ['पुरूष', 'माहिला'],
        datasets: [{
          data: [chartData.value['M'], chartData.value['F']],
          backgroundColor: [
            '#00B7D6',
            '#F1428A'
          ],
          hoverBackgroundColor: [
            '#00B7D6',
            '#F1428A'
          ]
        }]
      };

      this.registerPlugin();

      this.genderOptions = {
        // showAllTooltips: true
        legend: {
          labels: {
            generateLabels: (chart) => {
              return this.generateLegend(chart, false);
            }
          }
        }
      };
    }, error => {
      console.log('Error => ', error);
    });

    this._dashboardService.getSurnameData(args).subscribe(data => {
      this.surnameData = data;
      const chartData = this.generateChartData(this.surnameData, 'surname');

      this.surnameChartData = {
        labels: chartData.labels,
        datasets: [{
          label: 'Per Surname',
          backgroundColor: '#49AFD9',
          borderColor: '#0095D3',
          data: chartData.value
        }]
      };

      this.surnameOptions = {
        scaleShowValues: true,
        scales: {
          xAxes: [{
            ticks: {
              autoSkip: false
            }
          }]
        },
        legend: {
          labels: {
            generateLabels: (chart) => {
              return this.generateLegend(chart, false);
            }
          }
        }
      };
    }, error => {
      console.log('Error => ', error);
    });

    this._dashboardService.getSectionGenderData(args).subscribe(data => {
      const chartData = this.generateChartData(data, 'sectionGender');
      this.sectionGenderChartData = {
        labels: chartData.labels,
        datasets: [
          {
            label: 'पुरूष',
            backgroundColor: '#00B7D6',
            borderColor: '#00B7D6',
            data: chartData.value['M']
          },
          {
            label: 'माहिला',
            backgroundColor: '#F1428A',
            borderColor: '#F1428A',
            data: chartData.value['F']
          }
        ]
      };
      this.sectionGenderOptions = {
        scaleShowValues: true,
        scales: {
          xAxes: [{
            ticks: {
              autoSkip: false
            }
          }]
        },
        legend: {
          labels: {
            generateLabels: (chart) => {
              return this.generateLegend(chart, true);
            }
          }
        }
      };
    }, error => {
      console.log('Error => ', error);
    });

    this._dashboardService.getAgeVotersData(args).subscribe(data => {
      const chartData = this.generateChartData(data, 'ageVoters');
      this.ageVotersChartData = {
        labels: chartData.labels,
        datasets: [{
          label: 'Per Age Count',
          backgroundColor: '#49AFD9',
          borderColor: '#0095D3',
          data: chartData.value
        }]
      };
      this.ageVotersOptions = {
        legend: {
          labels: {
            generateLabels: (chart) => {
              return this.generateLegend(chart, false);
            }
          }
        }
      };
    }, error => {
      console.log('Error => ', error);
    });

    this._dashboardService.getHouseVotersData(args).subscribe(data => {
      this.houseVotersData = _.sortBy(data, ['sectionnamehindi']);
      this.rowGroupMetadata = {};
      if (this.houseVotersData) {
        for (let i = 0; i < this.houseVotersData.length; i++) {
          const rowData = this.houseVotersData[i];
          const sectionnamehindi = rowData.sectionnamehindi;
          if (i === 0) {
            this.rowGroupMetadata[sectionnamehindi] = { index: 0, size: 1 };
          } else {
            const previousRowData = this.houseVotersData[i - 1];
            const previousRowGroup = previousRowData.sectionnamehindi;
            if (sectionnamehindi === previousRowGroup) {
              this.rowGroupMetadata[sectionnamehindi].size++;
            } else {
              this.rowGroupMetadata[sectionnamehindi] = { index: i, size: 1 };
            }
          }
        }
      }
    }, error => {
      console.log('Error => ', error);
    });

    this._dashboardService.getAgeRangeVotersData(args).subscribe(data => {
      const chartData = this.generateChartData(data, 'ageRangeVoters');
      this.ageRangeVotersChartData = {
        labels: chartData.labels,
        datasets: [{
          label: 'Age Range Count',
          backgroundColor: ['#FF8142', '#00B7D6', '#FDD006', '#ED186F', '#318700'],
          data: chartData.value
        }]
      };
      this.ageRangeVotersOptions = {
        legend: {
          labels: {
            generateLabels: (chart) => {
              return this.generateLegend(chart, false);
            }
          }
        }
      };
    }, error => {
      console.log('Error => ', error);
    });
  }

  generateChartData = (data, type) => {
    const labels = [];
    const value = [];
    switch (type) {
      case 'gender':
        data.forEach(d => {
          switch (d.sex) {
            case 'M':
              d.sex = 'पुरूष';
              value['M'] = d.num;
              break;
            case 'F':
              d.sex = 'माहिला';
              value['F'] = d.num;
              break;
            default:
              break;
          }
          labels.push(d.sex);
        });
        break;
      case 'surname':
        data.forEach(d => {
          labels.push(d.surname);
          value.push(d.num);
        });
        break;
      case 'sectionGender':
        let femaleNum = [];
        let maleNum = [];
        data.forEach(d => {
          const section = _.findLast(data, ['SECTIONNO', d['SECTIONNO']]);
          d['gender'] = [
            {sex: section.sex, num: section.num},
            {sex: d.sex, num: d.num}
          ];
          delete d['sex'];
          delete d['num'];
          const index = _.indexOf(data, section);
          data.splice(index, 1);

          d.gender.forEach(g => {
            if (g.sex === 'F') {
              femaleNum = femaleNum.concat([g.num]);
            } else {
              maleNum = maleNum.concat([g.num]);
            }
          });
          labels.push(d.sectionnamehindi);
          value['F'] = femaleNum;
          value['M'] = maleNum;
        });
        break;
      case 'ageVoters':
        data = _.sortBy(data, ['age']);
        data.forEach(d => {
          labels.push(d.age);
          value.push(d.num);
        });
        break;
      case 'ageRangeVoters':
        Object.keys(data).forEach(key => {
          labels.push(key);
          value.push(data[key]);
        });
        break;
      default:
        break;
    }

    return {
      labels,
      value
    };
  }

  registerPlugin = () => {
    Chart.pluginService.register({
      beforeRender: function (chart) {
        if (chart.config.options.showAllTooltips) {
          // create an array of tooltips
          // we can't use the chart tooltip because there is only one tooltip per chart
          chart.pluginTooltips = [];
          chart.config.data.datasets.forEach(function (dataset, i) {
            chart.getDatasetMeta(i).data.forEach(function (sector, j) {
              chart.pluginTooltips.push(new Chart.Tooltip({
                _chart: chart.chart,
                _chartInstance: chart,
                _data: chart.data,
                _options: chart.options.tooltips,
                _active: [sector]
              }, chart));
            });
          });

          // turn off normal tooltips
          chart.options.tooltips.enabled = false;
        }
      },
      afterDraw: function (chart, easing) {
        if (chart.config.options.showAllTooltips) {
          // we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
          if (!chart.allTooltipsOnce) {
            if (easing !== 1) {
              return;
            }
            chart.allTooltipsOnce = true;
          }

          // turn on tooltips
          chart.options.tooltips.enabled = true;
          Chart.helpers.each(chart.pluginTooltips, function (tooltip) {
            tooltip.initialize();
            tooltip.update();
            // we don't actually need this since we are not animating tooltips
            tooltip.pivot();
            tooltip.transition(easing).draw();
          });
          chart.options.tooltips.enabled = false;
        }
      }
    });
  }

  generateLegend = (chart, multipleDataSet) => {
    const data = chart.data;

    if (data.labels.length && data.datasets.length) {
      if (multipleDataSet) {

        return data.labels.map(function(label, i) {
          const meta = chart.getDatasetMeta(0);
          const ds = data.datasets[0];
          const arc = meta.data[i];
          const custom = arc && arc.custom || {};
          const getValueAtIndexOrDefault = Chart.helpers.getValueAtIndexOrDefault;
          const arcOpts = chart.options.elements.arc;
          const fill = custom.backgroundColor ? custom.backgroundColor :
            getValueAtIndexOrDefault(ds.backgroundColor, i, arcOpts.backgroundColor);
          const stroke = custom.borderColor ? custom.borderColor : getValueAtIndexOrDefault(ds.borderColor, i, arcOpts.borderColor);
          const bw = custom.borderWidth ? custom.borderWidth : getValueAtIndexOrDefault(ds.borderWidth, i, arcOpts.borderWidth);

          // We get the value of the current label
          let multiValue = '';
          data.datasets.forEach((dataSet, index) => {
            multiValue += dataSet['label'] + ' : ';
            multiValue += dataSet['data'][i];
            if (index === 0) {
              multiValue += ', ';
            }
          });

          return {
              // Instead of `text: label,`
              // We add the value to the string
              text: label + ' (' + multiValue + ')',
              fillStyle: fill,
              strokeStyle: stroke,
              lineWidth: bw,
              hidden: isNaN(ds.data[i]) || meta.data[i].hidden,
              index: i
          };
        });
      } else {
        return data.labels.map(function(label, i) {
          const meta = chart.getDatasetMeta(0);
          const ds = data.datasets[0];
          const arc = meta.data[i];
          const custom = arc && arc.custom || {};
          const getValueAtIndexOrDefault = Chart.helpers.getValueAtIndexOrDefault;
          const arcOpts = chart.options.elements.arc;
          const fill = custom.backgroundColor ? custom.backgroundColor :
            getValueAtIndexOrDefault(ds.backgroundColor, i, arcOpts.backgroundColor);
          const stroke = custom.borderColor ? custom.borderColor : getValueAtIndexOrDefault(ds.borderColor, i, arcOpts.borderColor);
          const bw = custom.borderWidth ? custom.borderWidth : getValueAtIndexOrDefault(ds.borderWidth, i, arcOpts.borderWidth);

          // We get the value of the current label
          const value = chart.config.data.datasets[arc._datasetIndex].data[arc._index];

          return {
              // Instead of `text: label,`
              // We add the value to the string
              text: label + ' (' + value + ')',
              fillStyle: fill,
              strokeStyle: stroke,
              lineWidth: bw,
              hidden: isNaN(ds.data[i]) || meta.data[i].hidden,
              index: i
          };
        });
      }
    } else {
        return [];
    }
  }
}
