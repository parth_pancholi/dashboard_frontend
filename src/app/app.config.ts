import { Injectable } from '@angular/core';

@Injectable()
export class AppConfig {
  BASE_URL = {
    server: 'http://35.154.218.128:8080'
  };
  API_URL = {
    getMasterData: '/getMasterData',
    getBoothDetails: '/getBoothDetails',
    getGenderData: '/report1',
    getSurnameData: '/report2',
    getSectionGenderData: '/report3',
    getAgeVotersData: '/report4',
    getHouseVotersData: '/report5',
    getAgeRangeVotersData: '/report6'
  };
}
