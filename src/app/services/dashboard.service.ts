import { Injectable } from '@angular/core';
import { AppConfig } from '../app.config';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(
    private _appConfig: AppConfig,
    private _http: HttpClient
  ) { }

  getMasterData = (params) => {
    const url = this._appConfig.BASE_URL.server + this._appConfig.API_URL.getMasterData;
    return this._http.get(url);
  }

  getBoothDetails = (params) => {
    const url = this._appConfig.BASE_URL.server + this._appConfig.API_URL.getBoothDetails;
    return this._http.get(url + '/' + params.constituency + '/' + params.booth);
  }

  getGenderData = (params) => {
    const url = this._appConfig.BASE_URL.server + this._appConfig.API_URL.getGenderData;
    return this._http.get(url + '/' + params.constituency + '/' + params.booth);
  }

  getSurnameData = (params) => {
    const url = this._appConfig.BASE_URL.server + this._appConfig.API_URL.getSurnameData;
    return this._http.get(url + '/' + params.constituency + '/' + params.booth);
  }

  getSectionGenderData = (params) => {
    const url = this._appConfig.BASE_URL.server + this._appConfig.API_URL.getSectionGenderData;
    return this._http.get(url + '/' + params.constituency + '/' + params.booth);
  }

  getAgeVotersData = (params) => {
    const url = this._appConfig.BASE_URL.server + this._appConfig.API_URL.getAgeVotersData;
    return this._http.get(url + '/' + params.constituency + '/' + params.booth);
  }

  getHouseVotersData = (params) => {
    const url = this._appConfig.BASE_URL.server + this._appConfig.API_URL.getHouseVotersData;
    return this._http.get(url + '/' + params.constituency + '/' + params.booth);
  }

  getAgeRangeVotersData = (params) => {
    const url = this._appConfig.BASE_URL.server + this._appConfig.API_URL.getAgeRangeVotersData;
    return this._http.get(url + '/' + params.constituency + '/' + params.booth);
  }
}
